-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-05-2018 a las 19:10:39
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grutas`
--

CREATE TABLE `grutas` (
  `id` int(11) NOT NULL,
  `inicialr` int(11) NOT NULL,
  `finalr` int(11) NOT NULL,
  `distanciar` int(11) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `ciudad` varchar(45) NOT NULL,
  `nombrer` varchar(45) NOT NULL,
  `descripcionr` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grutas`
--

INSERT INTO `grutas` (`id`, `inicialr`, `finalr`, `distanciar`, `pais`, `ciudad`, `nombrer`, `descripcionr`) VALUES
(0, 123, 32, 2, 'Ecuador', 'Quito', 'ruta de las iglesias', 'Una ruta por la cual se conoce varias de las isgleias coloniales del centro de Quito'),
(1, 123, 32, 2, 'Ecuador', 'Quito', 'ruta de las iglesias', 'Una ruta por la cual se conoce varias de las isgleias coloniales del centro de Quito');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `grutas`
--
ALTER TABLE `grutas`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
