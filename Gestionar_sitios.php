<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sitio;
class Gestionar_sitios extends Controller
{
    public function index()
	{
		return \App\sitio::all();
	}
    public function show($id)
    {
    	return \App\sitio::find($id);
    }

    public function store(Request $request)
    {
    	//$objeto->campo = $request;
    	return  \App\sitio::create($request->all());
    }

    public function update(Request $request,$id)
    {
    	 $registro = \App\sitio::findOrFail($id);
    	 $registro->update($request->all());

    	 return $registro;
    }

    public function destroy($id)
    {
    	$registro = \App\sitio::findOrFail($id);
    	 $registro->delete();

    	 return 204;
    }
}
