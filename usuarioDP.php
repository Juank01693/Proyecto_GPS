<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;

class usuarioDP extends Controller
{
    //
    public function index()
    {
      return \App\usuario::all();
    }

    public function show($id)
    {
      return \App\usuario::find($id);
    }

    public function store(Request $request)
    {
      return \App\usuario::create($request->all());
    }

    public function update(Request $request, $id)
    {
      $registro = \App\usuario::findOrFail($id);
      $registro -> update($request->all());

      return $registro;
    }

    public function destroy($id)
    {
      $registro = \App\usuario::findOrFail($id);
      $registro -> delete($request->all());

      return 204;

    }
}
