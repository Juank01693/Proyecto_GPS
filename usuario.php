<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuario extends Model
{
    //
      public $timestamps = false;
      public $table = "usuario";
      protected $fillable = array('nombre', 'cedula/pasaporte', 'contraseña', 'correo');
}
