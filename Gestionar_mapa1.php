<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mapa;
class Gestionar_mapas extends Controller
{
    public function index()
	{
		return \App\mapa::all();
	}
    public function show($id)
    {
    	return \App\mapa::find($id);
    }

    public function store(Request $request)
    {
    	return  \App\mapa::create($request->all());
    }

    public function update(Request $request,$id)
    {
    	 $registro = \App\mapa::findOrFail($id);
    	 $registro->update($request->all());

    	 return $registro;
    }

    public function destroy($id)
    {
    	$registro = \App\mapa::findOrFail($id);
    	 $registro->delete();
    	 return 204;
    }
}
