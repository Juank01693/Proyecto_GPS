<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sitio extends Model
{
    public $timestamps = false;
     protected $table = "sitios";
     protected $fillable = array('nombre','latitud','longitud', 'descripcion');
}
