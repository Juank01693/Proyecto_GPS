import React from 'react';
import {
  ActivityIndicator,
  Button,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  
  TouchableOpacity,
  View,
} from 'react-native';
import main from '../../assets/main.png';
import { Content,Button as ButtonN, Text } from 'native-base';
import Exponent, { Constants, ImagePicker, registerRootComponent } from 'expo';
var style = require('./Styles.js');
export default class App extends React.Component {
  state = {
    image: null,
    uploading: false,
    worked:false,
  };
  componentDidMount(){
    
  }
  render() {
    let { image } = this.state;
    
    return (
      <View >
      {
        this.state.worked === false ?
            <View>
              <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={main}
              />
              </View>
              <ButtonN
                full
                block
                onPress={this._pickImage}
              >
                <Text>Elegir Imagen de galería</Text>
              </ButtonN>
                <View style={style.br}></View>
              <ButtonN
                full
                block
                onPress={this._takePhoto}
              >
                <Text>Tomar Foto</Text>
              </ButtonN>
            </View>
        :
        <View>
        {this._maybeRenderImage()}
        {this._maybeRenderUploadingOverlay()}
        </View>
      }
      </View>
    );
  }
  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: 'rgba(0,0,0,0.4)',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
          <ActivityIndicator color="#fff" animating size="large" />
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { image } = this.state;
    if (!image) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: '100%',
          justifyContent:'center',
          alignContent:'center',
          alignItems:'center'
        }}>
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            overflow: 'hidden',
          }}>
          <Image source={{ uri: image }} style={{ width: 250, height: 250 }} />
        </View>
        <Text>Su imagen no aplica para un descuento</Text>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <View style={style.br}></View>
        <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
          {image}
        </Text>
      </View>
    );
  };

  _share = () => {
    Share.share({
      message: this.state.image,
      title: 'Check out this photo',
      url: this.state.image,
    });
  };

  _copyToClipboard = () => {
    Clipboard.setString(this.state.image);
    alert('Se ha copiado el url de la imagen');
  };

  _takePhoto = async () => {
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _pickImage = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async pickerResult => {
    let uploadResponse, uploadResult;

    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        uploadResponse = await uploadImageAsync(pickerResult.uri);
        uploadResult = await uploadResponse.json();
        this.setState({ image: uploadResult.location ,worked:true});
      }
    } catch (e) {
      console.log({ uploadResponse });
      console.log({ uploadResult });
      console.log({ e });
      alert('Upload failed, sorry :(');
    } finally {
      this.setState({ uploading: false });
    }
  };
}

async function uploadImageAsync(uri) {
  let apiUrl = 'https://file-upload-example-backend-dkhqoilqqn.now.sh/upload';
  let uriParts = uri.split('.');
  let fileType = uri[uri.length - 1];

  let formData = new FormData();
  formData.append('photo', {
    uri,
    name: `photo.${fileType}`,
    type: `image/${fileType}`,
  });

  let options = {
    method: 'POST',
    body: formData,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
  };

  return fetch(apiUrl, options);
}