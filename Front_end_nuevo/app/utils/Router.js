import React,{Component} from 'react';
import { Router,Stack, Scene, Actions } from 'react-native-router-flux';
import Login from '../components/Login';
import LoginMail from '../components/LoginMail';
import First from '../components/First';
import Index from '../components/Index';
import Info from '../components/Info';
import Register from '../components/Register';


export default class RouterComponent extends Component{
render(){
    return (
      <Router>
        <Stack key="root">
            <Scene key="first" component={First} hideNavBar={true} initial={this.props.loggedIn?false:true}/>
            <Scene key="index" component={Index} hideNavBar={true} initial={this.props.loggedIn?true:false}/>
            <Scene key="login" component={Login} hideNavBar={true} />
            <Scene key="loginMail" component={LoginMail} hideNavBar={true} />
            <Scene key="register" component={Register} hideNavBar={true}/>
            <Scene key="info" component={Info} hideNavBar={true}/>
        </Stack>
      </Router>
    );
  }
}
