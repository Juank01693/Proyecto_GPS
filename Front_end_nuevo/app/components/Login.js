import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import { Actions, ActionConst } from 'react-native-router-flux';
import mini from '../../assets/mini.png';
import google from '../../assets/google.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/main.png';
var style = require('../utils/Styles.js');

export default class Login extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
    };
}
toLoginWithMail = () =>{
  Actions.loginMail();
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#f26a45' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
          <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
          </Body>
        </Header>
        <Content >
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={main}
              />
          </View>
          <Text style={{marginHorizontal:50,textAlign:'center'}}>Ingresa o regístrate para ganar descuentos visitando los lugares que te gustan</Text>
          <View style={style.br}></View>
          <TouchableHighlight style={style.customLoginBtn}>
            <View style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIconCustom} source={google} />
              <Text style={{textAlign:'center'}}>INGRESA CON GOOGLE</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={style.customLoginBtn}>
            <View style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Image resizeMode="contain" style={style.miniIconCustom} source={facebook} />
              <Text style={{textAlign:'center'}}>INGRESA CON FACEBOOK</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight style={style.RegisterBtn} onPress={this.toLoginWithMail}>
              <Text style={{textAlign:'center',color:'#fff'}}>INICIAR SESIÓN</Text>
          </TouchableHighlight>
        </Content>
      </Container>
    );
  }
}
