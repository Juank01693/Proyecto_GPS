import React,{Component} from 'react';
import { AppRegistry, View, Image,TouchableHighlight, KeyboardAvoidingView,AsyncStorage ,Alert, WebView, Linking, Platform } from 'react-native';
import { Container, Label,Header, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import {logInUser,verifyUser} from '../utils/Controller';
import mini from '../../assets/mini.png';
import main from '../../assets/main.png';
var style = require('../utils/Styles.js');

export default class Register extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        email:'',
        password:'',
    };
}
toRegister = () =>{
  console.log('register');
  Actions.register();
}
login = () =>{
    this.setState({isLoading:true});
    let email = this.state.email;
    let self  = this;
    let password = this.state.password;
    logInUser(email, password).then(async data => {
        console.log('data: ', data);
        try {
            let exists = await verifyUser(data.user.uid);
            if(exists){
                await AsyncStorage.setItem('uid', data.user.uid);
                self.setState({isLoading:false});
                Actions.index();
            }
            else {
                self.setState({isLoading:false});
                Alert.alert('Error','Datos incorrectos');
            }
        } catch (error) {
            console.log('error saving uid: ', error);
            self.setState({isLoading:false});
        }
       
    }).catch((error) => {
        console.log('firebaseError',error);
        self.setState({isLoading:false});
        Alert.alert('Error',error.message);
    });
}
render(){
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner color='#f26a45' />
          </View>
      )
    }
    return (
      <Container>
        <Header>
            <Left>
                <Button transparent onPress={ ()=>{Actions.pop()} }>
                    <Icon name='ios-arrow-back' style={{color:'#fff'}}/>
                </Button>
            </Left>
            <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
                <Image resizeMode="contain" style={style.miniIcon} source={mini} /><Title>RUMBO 360</Title>
            </Body>
        </Header>
        <Content >
          <View style={style.mainImageContainer}>
              <Image
                  resizeMode="contain"
                  style={style.mainImage}
                  source={main}
              />
          </View>
          <KeyboardAvoidingView>
          <Form>
            <Item floatingLabel style={style.paddingTextCustom}>
                <Input autoCorrect={ false }   keyboardType='email-address'  autoCapitalize = 'none' placeholder='Correo electrónico' onChangeText={ (email) => this.setState({ email }) }/>
            </Item>
            <Item floatingLabel last style={style.paddingTextCustom}>
                <Input placeholder='Contraseña' onChangeText={ (password) => this.setState({ password }) } secureTextEntry />
            </Item>
                {  
                this.state.isLoading ?
                <Spinner size='small' color='black' />
                :
                <View>
                    <Button block  style={style.paddingTextCustom} onPress={ this.login }>
                        <Text>Iniciar Sesión</Text>
                    </Button>
                    <Button block transparent style={style.paddingTextCustom} onPress={ this.toRegister }>
                        <Text>¿No tienes una cuenta? Regístrate.</Text>
                    </Button>
                </View>
                }
            </Form>
        </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}
