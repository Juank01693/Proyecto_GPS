<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Gestionar_Rutas extends Controller
{
     public function index()
    {
    	return \App\rutas::all();

    }

      public function shows($id)
    {
    	return \App\rutas::find($id);

    }


    public function store(Request $request,$id )
    {
    	return  \App\rutas::create($request->all());

    }

    public function edit(Request $request,$id)
    {

    	$registro=\App\rutas::finOrFail($id);
    	$registro-> update($request-> all());

    	return $registro;


    }

    public function destroy($id)
    {

    	$registro=\App\rutas::finOrFail($id);
    	$registro-> delete();


    	return 204;


    }
}
