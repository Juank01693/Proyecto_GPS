<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//http://localhost:8000/api/nombreruta
Route::resource('sitio','Gestionar_sitios');

//http://localhost:8000/api/nombreruta
Route::resource('usuario','Gestionar_tour');

//http://localhost:8000/api/nombreruta
Route::resource('usuario','Gestionar_mapa');

Route::resource('Rutas','Gestionar_Rutas');