<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::get('/users','ApiUsers@index');
Route::get('/users/{id}','ApiUsers@show');
Route::post('/users','ApiUsers@store');
Route::put('/users/{id}','ApiUsers@update');
Route::delete('/users/{id}','ApiUsers@delete');



Route::get('/places','ApiPlaces@index');
Route::get('/places/{id}','ApiPlaces@show');
Route::post('/places','ApiPlaces@store');
Route::put('/places/{id}','ApiPlaces@update');
Route::delete('/places/{id}','ApiPlaces@delete');

Route::get('/users_places','ApiUsersPlaces@index');
Route::get('/users_places/{id}','ApiUsersPlaces@show');
Route::post('/users_places','ApiUsersPlaces@store');
Route::put('/users_places/{id}','ApiUsersPlaces@update');
Route::delete('/users_places/{id}','ApiUsersPlaces@delete');